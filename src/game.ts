/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/

import {spawnPlaneX, spawnBoxX, spawnGltfX, spawnCone} from './modules/spawnerFunctions'
import { GroundCover } from './modules/GroundCover'

const groundMaterial = new Material()
//groundMaterial.albedoColor = new Color3(0.1, 0.4, 0)
groundMaterial.albedoTexture = new Texture("materials/grassy-512-1-0.png")

let ground = new GroundCover(0,0,16,0.01,16,groundMaterial,true)

const box = spawnBoxX(5,0.5,5, 0,0,0,  1,1,1)
const boxMaterial = new Material()
boxMaterial.albedoColor = new Color3(0,0.2,0)
boxMaterial.metallic = 1
boxMaterial.roughness = 0
box.addComponent(boxMaterial)

const cube = spawnGltfX(new GLTFShape("models/Lesson9Cube/Lesson9Cube.glb"), 5,0,6, 0,0,0, 1,1,1)
const tree = spawnGltfX(new GLTFShape("models/Tree/Tree.gltf"), 7,0,5, 0,0,0,  0.5,0.5,0.5)
const tree2 = spawnGltfX(new GLTFShape("models/Tree/Tree.gltf"), 4,0,5, 0,0,0,  0.5,0.5,0.5)
const grassTuft = spawnGltfX(new GLTFShape("models/GrassTuft/GrassTuft.gltf"), 7,0,3, 0,0,0,  0.5,0.5,0.5)
const firTree = spawnGltfX(new GLTFShape("models/TreeA_Fir/TreeA_Optimised_28_June_2018_A01.babylon.gltf"), 2,0,2, 0,0,0,  0.05,0.05,0.05)
//const bird = spawnGltfX(new GLTFShape("models/Sparrow/Sparrow-burung-pipit.gltf"), 4,4,5, 0,0,0, 0.05, 0.05, 0.05)

const birdShape = new GLTFShape('models/Sparrow/Sparrow-burung-pipit.gltf')
const bird = new Entity()
bird.addComponent(birdShape)
const TransformBird = new Transform({
  position: new Vector3(4,4,5),
  rotation: Quaternion.Euler(0,0,0),
  scale: new Vector3(0.05,0.05,0.05)
})
bird.addComponent(TransformBird)

// Create animator component
let animator = new Animator()

// Add animator component to the entity
bird.addComponent(animator)

// Instance animation clip object
const clipFly = new AnimationState("fly")

// Add animation clip to Animator component
animator.addClip(clipFly)
clipFly.speed = 2

const ambienceclip = new AudioClip('sounds/forest_ambience.mp3')
const ambiencesource = new AudioSource(ambienceclip)
tree.addComponent(ambiencesource)
ambiencesource.playing = true

const birdclip = new AudioClip('sounds/falcon.mp3')
const birdsource = new AudioSource(birdclip)
bird.addComponent(birdsource)

@Component("birdlerpdata")
export class BirdLerpData {
  array: Vector3[]
  origin: number = 0
  target: number = 1
  fraction: number = 0
  constructor(path: Vector3[]) {
    this.array = path
  }
}

bird.addComponent(new BirdLerpData([
  new Vector3(4,4,5), 
  new Vector3(7,4,5),
  new Vector3(2,0,2),
  new Vector3(4,4,5)
]))
engine.addEntity(bird)
birdsource.playing = true //only chirp once 
export class FlyingBird {

  update(dt: number) {

    let speedFactor=2 //the lower the number, the faster the speed        
    let transform = bird.getComponent(Transform)
    let path = bird.getComponent(BirdLerpData)
   
    if ((transform.position.x==4 && transform.position.z==5) || (transform.position.x==7 && transform.position.z==5) || (transform.position.x==2 && transform.position.z==2)) {
        clipFly.stop()
        let timeout=2
        setTimeout(() => {
            
        }, timeout);
    } else { 
        clipFly.play()
    }

    if (path.fraction < 1) {
        path.fraction += dt/speedFactor
        transform.position = Vector3.Lerp(path.array[path.origin],path.array[path.target],path.fraction) 
    } else {
        path.origin = path.target
        path.target += 1
    
            if (path.target >= path.array.length) {
              path.target = 0
            }
    
        path.fraction = 0
        transform.lookAt(path.array[path.target]) 
    }

        if (path.target >= path.array.length) {
            path.target = 0
            path.fraction = 0
            birdsource.playing = false
        }
    }
} 

engine.addSystem(new FlyingBird())