//////////////////////////////////////////
// Simple Waypoint System
// (c) 2019 Carl Fravel
// You are welcome to use any content from this open source scene in compliance with the license.
/*
Copyright 2019 Carl Fravel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// An instructive example of using this module can be found at https://gitlab.com/dcl-tech/demo-simplewaypointsystem
// One way to use this module:
// 1. Create the scene, in the DCL Builder and/or the SDK until you are at the point you are ready to set up the path for the moving object.
// 2. Add the spawnerFunctions, BuilderHUD and SimpleWaypointSystem ts files to your src/modules directory, and import statements for them in your script.
// 3. Use the BuilderHUD to place each of the waypoints.
// 4. Do a Save in the BuilderHUD
// 5. Copy the Transform parametric info from the F12 console and paste it into your scene, as parameters to calls to my SpawnerFunctions  module's spawnGltfX() function, which takes the model shape and the 9 parameters of Position,  Rotation and Scale.  Use the XYX widget for the waypoints, like in my SimpleWaypointSystem demo example.
// 6. Preview the scene to check that the waypoint widgets are in the right places and, if desired, oriented the right way.
// 7. Instantiate the SimpleWaypointSystem in your scene
// 8. call pushWaypoint() on it for each waypoint you made, in the order you want them traversed.
// 9. Set the other properties of your waypoint system to get the behaviour you want.
//    for example, initially you may want to have removeWaypointEntities=false to leave the waypoints visible in the scene, like they are in the demo scene.  Later set that to true so that the waypoints disappear when they are submitted to pushWaypoint.
// 10. If you want to receive an event at each waypoint, use the EventManager and listener pattern you see in the demo.
// 11. Have fun!
// 
// If you want to have multiple entities being moved, create an instance of this class for each.
// These instances could use the same or different waypoints pushed to them, although only the last should use removeWaypointEntities=true
//

export { SimpleWaypointSystem, WaypointIdle, WaypointTraveling }


@EventConstructor()
class WaypointIdle {
  constructor(/*public waypointSystem: SimpleWaypointSystem, public eventManager: EventManager, */public traveler: Entity) {}
}

@EventConstructor()
class WaypointTraveling {
  constructor(/*public waypointSystem: SimpleWaypointSystem, public eventManager: EventManager, */public traveler: Entity) {}
}


class SimpleWaypointSystem implements ISystem {

  //////////////////////////////////////////
  // Properties

  traveler: Entity //This is the entity being moved. You can have multiple instances to handle different entities
  eventManager: EventManager // Events will be issued to this EventManager in the scene.

  // Waypoints.  Typically inserted in an empty initial list using addWaypoint.  Alternatively the locations, orientations and numWaypoints can be set
  locations: Vector3[] = []
  orientationsQ: Quaternion[] = []
  numWaypoints: number = 0

  removeWaypointEntities:boolean = true // if true, the waypoint entities used in the addWaypoint method should be removed from the engine (made invisible)
  delayTime: number = 1 //sec
  travelTime: number = 1  //sec
  haltUntilTriggered: boolean = false
  loop: boolean = true // should the travel loop back to first waypoint, or halt at the last waypoint
  orientToNextWaypoint: boolean = false  // if true, then the entity will be faced toward the next waypoint during each segment. If false, orientation is interpolated

  // internal
  currentIdleTime: number = 0.0 // count down timer, in seconds, for how much more time to idle when in idle mode
  currentSegmentTravelTime: number = 0.0 // count town timer, in seconds, for how much mor time to travel, when in travelling mode.
  isTraveling: boolean = true
  wpIndexBegin: number = 0 // the zero-based index of the starting waypoint of the current travel segement 
  wpIndexEnd: number = 1 // the zero-based index of the starting waypoint of the current travel segement 
  justStarted: boolean = true

  dx: number = 0.0
  dy: number = 0.0
  dz: number = 0.0
  drxQ: number = 0.0
  dryQ: number = 0.0
  drzQ: number = 0.0
  drwQ: number = 0.0

  constructor(traveler: Entity, eventManager: EventManager) {
      this.traveler = traveler
      this.eventManager = eventManager
  }

  // Use this to attach the waypoint to an existing entity that has a Transform
  // The waypoint will track that transform, including if it is changed in the scene.
  pushWaypoint(wpE: Entity) {
    if (wpE == null) return
    if (this.removeWaypointEntities) {
      engine.removeEntity(wpE) // if the waypoint entities should be removed from the engine (made invisible)
    }
    try {
      let transform = wpE.getComponent(Transform)
      this.locations[this.numWaypoints] = transform.position
      this.orientationsQ[this.numWaypoints] = transform.rotation
      this.numWaypoints++
    }
    catch (e){
      // wpE had no transform, so do nothing for this waypoint
    }
  }

  // Use this if what matters is just the location and orientation.
  pushWaypointCoordinates(x:number,y:number,z:number,   rX:number,rY:number,rZ:number){
    this.locations[this.numWaypoints] = new Vector3(x, y, z)
    this.orientationsQ[this.numWaypoints] = Quaternion.Euler(rX,rY,rZ)
    this.numWaypoints++
  }

  getNextWaypoint(loc1: number) {
    // Pick a destination location from locations[], but not the current loc1
    let wpIndex : number = loc1 + 1
    while (wpIndex >= this.locations.length) {
      wpIndex -= this.locations.length // wrap around from beginning
    }
    return wpIndex
  }

  // release the traveler from an idle, whether or not it is in haltUntilTriggered mode.
  trigger() {
    //!CF TODO implement
    if (!this.isTraveling) {
      this.currentIdleTime = 0
    }
  }

  reset(wpStart:number) {
    if ((this.numWaypoints < 2)||(wpStart < 0) || (wpStart > this.numWaypoints-1))
      return // reset does nothing if there are not yet at least 2 waypoints or if the number is out of range
    
    this.justStarted = true
    this.wpIndexBegin = wpStart  
    this.wpIndexEnd = this.getNextWaypoint(wpStart)

    this.moveTravelerToWaypoint(this.locations[this.wpIndexBegin], this.orientationsQ[this.wpIndexBegin])

    if (this.isTraveling) {
      this.currentIdleTime = 0
      this.currentSegmentTravelTime = this.travelTime
      this.eventManager.fireEvent(new WaypointTraveling(/*this, this.eventManager, */this.traveler))
    } 
    else {
      this.currentIdleTime = 0
      this.currentSegmentTravelTime = this.travelTime
      this.eventManager.fireEvent(new WaypointIdle(/*this, this.eventManager, */this.traveler))
    }
  }

  activate(engine: IEngine) {
    this.reset(0)
  }
    
  moveTravelerToWaypoint(pos:Vector3, rotQ:Quaternion) {
    /*
    // this seems to work the first loop around, but then gets weird.  //!CF TODO figure out why?
    this.traveler.getComponent(Transform).position=pos
    if (!this.orientToNextWaypoint) {
      this.traveler.getComponent(Transform).rotation=rotQ
    }
    */

    let objectPos = this.traveler.getComponent(Transform).position
    objectPos.x = pos.x
    objectPos.y = pos.y
    objectPos.z = pos.z
    
    if (!this.orientToNextWaypoint) {
      let objectRotQ = this.traveler.getComponent(Transform).rotation
      objectRotQ.x = rotQ.x 
      objectRotQ.y = rotQ.y
      objectRotQ.z = rotQ.z
      objectRotQ.w = rotQ.w
    }
  }
  

  // Start the idling state
  startIdling() {
    this.isTraveling = false
    this.currentIdleTime = this.delayTime
     if (!this.justStarted) {
      // Set the traveler exactly at the position and orientation of the waypoint just arrived at
      this.moveTravelerToWaypoint(this.locations[this.wpIndexEnd], this.orientationsQ[this.wpIndexEnd])
      // Set up the presumed next travel segment
      this.wpIndexBegin = this.wpIndexEnd // new starting location will be the endpoint of this flight
      this.wpIndexEnd = this.getNextWaypoint(this.wpIndexBegin) // get destination for next leg of flight, but wrap it to first wp if needed.
    }
    this.eventManager.fireEvent(new WaypointIdle(this.traveler))
  }


  startTraveling(dt: number) { // Assumes wpIndexBegin and wpIndexEnd have been set for the segment
    // set the value interpolation factor 
    let dvdt = dt / this.travelTime // use the dt of this frame as typical.  probably about 1/30 sec or about 0.033 sec

    // set up the new travel segment
    let wpBeginPos = this.locations[this.wpIndexBegin]
    let wpEndPos = this.locations[this.wpIndexEnd]
    let wpBeginRotQ = this.orientationsQ[this.wpIndexBegin]
    let wpEndRotQ = this.orientationsQ[this.wpIndexEnd]

    // calculate the increments of travel distance and rotation per frame 
    this.dx = (wpEndPos.x - wpBeginPos.x) * dvdt
    this.dy = (wpEndPos.y - wpBeginPos.y) * dvdt
    this.dz = (wpEndPos.z - wpBeginPos.z) * dvdt

    if (!this.orientToNextWaypoint) {
      this.drxQ = (wpEndRotQ.x - wpBeginRotQ.x) * dvdt
      this.dryQ = (wpEndRotQ.y - wpBeginRotQ.y) * dvdt
      this.drzQ = (wpEndRotQ.z - wpBeginRotQ.z) * dvdt
      this.drwQ = (wpEndRotQ.w - wpBeginRotQ.w) * dvdt
    }
    this.currentSegmentTravelTime = this.travelTime
    
    // Move the object to start point, in case it isn't currently there (e.g. fron its original instantiation)
    this.moveTravelerToWaypoint(wpBeginPos, wpBeginRotQ)
    if (this.orientToNextWaypoint) {
      /* 
      // courtesy RDixon:
      // var angles:Vector3 = myEntity.get(Transform).rotation.eulerAngles;
      // angles.y = Math.atan2(this.origin.x - this.destination.x, this.origin.z - this.destination.z)*180 / Math.PI;
      // myEntity.get(Transform).rotation.eulerAngles = angles;
      */
      var angles:Vector3 = this.traveler.getComponent(Transform).rotation.eulerAngles
      angles.y = Math.atan2(wpEndPos.x- wpBeginPos.x, wpEndPos.z - wpBeginPos.z)*180 / Math.PI
      this.traveler.getComponent(Transform).rotation.eulerAngles = angles
    }
    
    // Start the animation, if any //!CF TODD enable Wayppint System to trigger an animation on each travel segment
    
    this.isTraveling = true
    this.eventManager.fireEvent(new WaypointTraveling(this.traveler))
  }

  update(dt: number) {
    if (this.numWaypoints < 2) return // don't do anything until/unless there are 2 or more waypoints
    if (dt < 0.001) return // the dt in the very first update may be almost zero for some reason, wait for a valid frame duration.  In general skip any tiny ones.
    if (this.isTraveling) {
      if (this.justStarted) {
        this.startTraveling(0.034)
        this.justStarted = false
      }
      if (this.currentSegmentTravelTime >= dt){
        // Travel
        this.currentSegmentTravelTime -= dt
        // move the object by dx, dy, dz
        let objectPos = this.traveler.getComponent(Transform).position
        objectPos.x += this.dx
        objectPos.y += this.dy
        objectPos.z += this.dz
        if (!this.orientToNextWaypoint) {
          // rotate the object by dr quaternian
          let rotVectorQ = this.traveler.getComponent(Transform).rotation
          this.traveler.getComponent(Transform).rotation.set(rotVectorQ.x + this.drxQ, rotVectorQ.y + this.dryQ, rotVectorQ.z + this.drzQ, rotVectorQ.w + this.drwQ)
        }
      }
      else {
        this.startIdling()
      }
    }
    else
    {
      if (this.justStarted) {
        this.startIdling()
        this.justStarted = false
      }
      // Wait until this.currentIdleTime is decremented to zero
      if (this.currentIdleTime > 0) { //idle time has expired
        if (!this.haltUntilTriggered) {
          this.currentIdleTime -= dt // otherwise just idle until/unless triggered
        }
      }
      else {
        this.startTraveling(dt)
      }
    }
  }
}
  
  