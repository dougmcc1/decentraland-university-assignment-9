///////////////////////////////////////////////////////
// DirectionPanelsDisplay
// You are welcome to use any content from this open source scene in compliance with the license.
/*
Copyright 2019 Carl Fravel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// This is an entity consisting of 8 thin boxes and 8 planes, each facing toward a compass direction,
// with texture images applied that indicate the direction the panel is facing.
// Demonstrates the right rotations for Boxes and Planes that will have Textures on them
// Stand inside the circles of 8 pairs of panels to see the direction that the faces are facing
// For example, the panels farthest north show a south-facing image as seen from the center, hence is "S."
//
// Also allows testing of the standoff gap that a textured box or plane must have from the object next further away to avoid flicker.
//
// When you are putting up signs and posters in a scene, you can copy/paste to use the values below as coordinates templates, 
// and use a DF variable in your own code to adjust the standoff distance based on how far you want to see them without flicker.
// 
// Technical notes:
// Adds itself to the engine
// Dependencies:
//  materials/nsew images
//  models/xyz glb file

import {spawnGltfX, spawnPlaneX, spawnBoxX} from './SpawnerFunctions'
export{DirectionsPanelsDisplay}

class DirectionsPanelsDisplay extends Entity {
    //DF:number = 0.001 // flickering starts within the same parcel
    //DF:number = 0.002 // at about a parcel from the entity
    //DF:number = 0.005 // at about 18m
    //DF:number = 0.01  // at about 28m
    //DF:number = 0.02  // at about 40-45m
    //DF:number = 0.05  // at about 64m (about fog distance or 4 parcels away in 6.2.2)

    boxZ:number = 0.001 // thickness of the thin box panels

    tN: Texture
    tNE: Texture
    tE: Texture
    tSE: Texture
    tS: Texture
    tSW: Texture
    tW: Texture
    tNW: Texture
    
    mN: Material
    mNE: Material
    mE: Material
    mSE: Material
    mS: Material
    mSW: Material
    mW: Material
    mNW: Material

    mBack: Material
    
    // boxes
    bS: Entity
    bSW: Entity
    bW: Entity
    bNW: Entity
    bN: Entity
    bNE: Entity
    bE: Entity
    bSE: Entity
    
    // backing boxes
    bbS: Entity
    bbSW: Entity
    bbW: Entity
    bbNW: Entity
    bbN: Entity
    bbNE: Entity
    bbE: Entity
    bbSE: Entity
    
    // planes
    pS: Entity
    pSW: Entity
    pW: Entity
    pNW: Entity
    pN: Entity
    pNE: Entity
    pE: Entity
    pSE: Entity
        
    // backing planes
    bpS: Entity
    bpSW: Entity
    bpW: Entity
    bpNW: Entity
    bpN: Entity
    bpNE: Entity
    bpE: Entity
    bpSE: Entity
        
    // xyz widget to show direction in which this entity is set up
    xyzShape: GLTFShape
    xyz: Entity
    backingPanels:boolean
    
    constructor(DF:number = 0.01, backingPanels:boolean = true) {
        super()
        engine.addEntity(this)

        this.backingPanels = backingPanels
        log("backingPanels =", this.backingPanels)
        let boxZ = this.boxZ

        this.tN = new Texture ('materials/nsew/N.png')
        this.tNE = new Texture ('materials/nsew/NE.png')
        this.tE = new Texture ('materials/nsew/E.png')
        this.tSE = new Texture ('materials/nsew/SE.png')
        this.tS = new Texture ('materials/nsew/S.png')
        this.tSW = new Texture ('materials/nsew/SW.png')
        this.tW = new Texture ('materials/nsew/W.png')
        this.tNW = new Texture ('materials/nsew/NW.png')
        
        this.mN = new Material ()
        this.mNE = new Material ()
        this.mE = new Material ()
        this.mSE = new Material ()
        this.mS = new Material ()
        this.mSW = new Material ()
        this.mW = new Material ()
        this.mNW = new Material ()

        this.mN.albedoTexture = this.tN
        this.mNE.albedoTexture = this.tNE
        this.mE.albedoTexture = this.tE
        this.mSE.albedoTexture = this.tSE
        this.mS.albedoTexture = this.tS
        this.mSW.albedoTexture = this.tSW
        this.mW.albedoTexture = this.tW
        this.mNW.albedoTexture = this.tNW
        
        this.mBack = new Material()
        this.mBack.albedoColor = new Color3(0,0,0)
        
        let sqrt2 = 1.4142
        ///////////////// Thin-Box panels
        // reverse side is upside down (up/down reversed)
        this.bS = spawnBoxX(8,1,12-DF, 0,0,0, 1.3, 1, boxZ)
        this.bS.setParent(this)
        this.bSW = spawnBoxX(8+2*sqrt2-DF/sqrt2,1,8+2*sqrt2-DF/sqrt2, 0,45,0, 1.3, 1, boxZ) // good
        this.bSW.setParent(this)
        this.bW = spawnBoxX(12-DF,1,8, 0,90,0, 1.3, 1, boxZ)
        this.bW.setParent(this)
        this.bNW = spawnBoxX(8+2*sqrt2-DF/sqrt2,1,8-2*sqrt2+DF/sqrt2, 0,135,0, 1.3, 1, boxZ)
        this.bNW.setParent(this)
        this.bN = spawnBoxX(8,1,4+DF, 0,180,0, 1.3, 1, boxZ)
        this.bW.setParent(this)
        this.bNE = spawnBoxX(8-2*sqrt2+DF/sqrt2,1,8-2*sqrt2+DF/sqrt2, 0,-135,0, 1.3, 1, boxZ)
        this.bNE.setParent(this)
        this.bE = spawnBoxX(4+DF,1,8, 0,-90,0, 1.3, 1, boxZ)
        this.bE.setParent(this)
        this.bSE = spawnBoxX(8-2*sqrt2+DF/sqrt2,1,8+2*sqrt2-DF/sqrt2, 0,-45,0, 1.3, 1, boxZ)  // good
        this.bSE.setParent(this)
        
        this.bN.addComponent(this.mN)
        this.bNE.addComponent(this.mNE)
        this.bE.addComponent(this.mE)
        this.bSE.addComponent(this.mSE)
        this.bS.addComponent(this.mS)
        this.bSW.addComponent(this.mSW)
        this.bW.addComponent(this.mW)
        this.bNW.addComponent(this.mNW)

        // Optional backing boxes
        if (this.backingPanels) {
            this.bbS = spawnBoxX(8,1,12, 0,0,0, 1.3, 1, boxZ)
            this.bbS.setParent(this)
            this.bbSW = spawnBoxX(8+2*sqrt2,1,8+2*sqrt2, 0,45,0, 1.3, 1, boxZ)
            this.bbSW.setParent(this)
            this.bbW = spawnBoxX(12,1,8, 0,90,0, 1.3, 1, boxZ)
            this.bbW.setParent(this)
            this.bbNW = spawnBoxX(8+2*sqrt2,1,8-2*sqrt2, 0,135,0, 1.3, 1, boxZ)
            this.bbNW.setParent(this)
            this.bbN = spawnBoxX(8,1,4, 0,180,0, 1.3, 1, boxZ)
            this.bbW.setParent(this)
            this.bbNE = spawnBoxX(8-2*sqrt2,1,8-2*sqrt2, 0,-135,0, 1.3, 1, boxZ)
            this.bbNE.setParent(this)
            this.bbE = spawnBoxX(4,1,8, 0,-90,0, 1.3, 1, boxZ)
            this.bbE.setParent(this)
            this.bbSE = spawnBoxX(8-2*sqrt2,1,8+2*sqrt2, 0,-45,0, 1.3, 1, boxZ)
            this.bbSE.setParent(this)

            this.bbN.addComponent(this.mBack)
            this.bbNE.addComponent(this.mBack)
            this.bbE.addComponent(this.mBack)
            this.bbSE.addComponent(this.mBack)
            this.bbS.addComponent(this.mBack)
            this.bbSW.addComponent(this.mBack)
            this.bbW.addComponent(this.mBack)
            this.bbNW.addComponent(this.mBack)
        }
        
        /////////////// Plane Panels
        //reverse side is mirror image (left/right reversed)
        this.pS = spawnPlaneX(8,2.1,12-DF, 180,180,0, 1.3, 1, 1)
        this.pS.setParent(this)
        this.pSW = spawnPlaneX(8+2*sqrt2-DF/sqrt2,2.1,8+2*sqrt2-DF/sqrt2, 180,135,0, 1.3, 1, 1)
        this.pSW.setParent(this)
        this.pW = spawnPlaneX(12-DF,2.1,8, 180,90,0, 1.3, 1, 1)
        this.pW.setParent(this)
        this.pNW = spawnPlaneX(8+2*sqrt2-DF/sqrt2,2.1,8-2*sqrt2+DF/sqrt2, 180,45,0, 1.3, 1, 1)
        this.pNW.setParent(this)
        this.pN = spawnPlaneX(8,2.1,4+DF, 180,0,0, 1.3, 1, 1)
        this.pN.setParent(this)
        this.pNE = spawnPlaneX(8-2*sqrt2+DF/sqrt2,2.1,8-2*sqrt2+DF/sqrt2, 180,-45,0, 1.3, 1, 1)
        this.pNE.setParent(this)
        this.pE = spawnPlaneX(4+DF,2.1,8, 180,-90,0, 1.3, 1, 1)
        this.pE.setParent(this)
        this.pSE = spawnPlaneX(8-2*sqrt2+DF/sqrt2,2.1,8+2*sqrt2-DF/sqrt2, 180,-135,0, 1.3, 1, 1)
        this.pSE.setParent(this)

        this.pN.addComponent(this.mN)
        this.pNE.addComponent(this.mNE)
        this.pE.addComponent(this.mE)
        this.pSE.addComponent(this.mSE)
        this.pS.addComponent(this.mS)
        this.pSW.addComponent(this.mSW)
        this.pW.addComponent(this.mW)
        this.pNW.addComponent(this.mNW)

        //optional backing planes
        if (this.backingPanels){
            this.bpS = spawnPlaneX(8,2.1,12, 180,180,0, 1.3, 1, 1)
            this.bpS.setParent(this)
            this.bpSW = spawnPlaneX(8+2*sqrt2,2.1,8+2*sqrt2, 180,135,0, 1.3, 1, 1)
            this.bpSW.setParent(this)
            this.bpW = spawnPlaneX(12,2.1,8, 180,90,0, 1.3, 1, 1)
            this.bpW.setParent(this)
            this.bpNW = spawnPlaneX(8+2*sqrt2,2.1,8-2*sqrt2, 180,45,0, 1.3, 1, 1)
            this.bpNW.setParent(this)
            this.bpN = spawnPlaneX(8,2.1,4, 180,0,0, 1.3, 1, 1)
            this.bpN.setParent(this)
            this.bpNE = spawnPlaneX(8-2*sqrt2,2.1,8-2*sqrt2, 180,-45,0, 1.3, 1, 1)
            this.bpNE.setParent(this)
            this.bpE = spawnPlaneX(4,2.1,8, 180,-90,0, 1.3, 1, 1)
            this.bpE.setParent(this)
            this.bpSE = spawnPlaneX(8-2*sqrt2,2.1,8+2*sqrt2, 180,-135,0, 1.3, 1, 1)
            this.bpSE.setParent(this)

            this.bpN.addComponent(this.mBack)
            this.bpNE.addComponent(this.mBack)
            this.bpE.addComponent(this.mBack)
            this.bpSE.addComponent(this.mBack)
            this.bpS.addComponent(this.mBack)
            this.bpSW.addComponent(this.mBack)
            this.bpW.addComponent(this.mBack)
            this.bpNW.addComponent(this.mBack)
            }

        
        this.xyzShape = new GLTFShape('models/xyz/xyzLeftHand.glb')
        this.xyz = spawnGltfX(this.xyzShape, 8,1,8, 0,180,0,  0.2,0.2,0.2)
        this.xyz.setParent(this)
    }
}

