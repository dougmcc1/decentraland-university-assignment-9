//////////////////////////////////
// GroundCover system
// DCL Conference Center Utlities
// You are welcome to use any content from this open source scene in compliance with the license.
/*
Copyright 2019 Carl Fravel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

export {GroundCover, GroundCoverPath, GroundCoverCrossPath}

const minY = 0.001 // min height of the ground pieces, so that 0, can be passed in to mean minimum

/////////////////////////////////////
// GroundCover will always keep x and z as whole numbers >= 1
// If <1, makes them 1, then apply Math.floor
// @CF todo above
class GroundCover extends Entity {
    constructor (x:number, z:number, xWidth:number, yHeight:number, zDepth:number, mtl:Material, addToEngine:boolean){
        super();
        if (yHeight < minY) {
            yHeight = minY
        }

        let shape = new BoxShape()
        //shape.withCollisions=true //@CF TODO
        let transform = new Transform()
        transform.position = new Vector3(x+xWidth/2, yHeight/2, z+zDepth/2)
        transform.scale = new Vector3(xWidth, yHeight, zDepth)
        this.addComponent(transform)
        this.addComponent(shape)
        this.addComponent(mtl)
        if (addToEngine) {
            engine.addEntity(this)
        }

/*
        // Locate this entity so that the x,y is at the sw corner of this GroundCover, and xWidth and zDepth will be to the East and North
        const transform = new Transform({ position: new Vector3(x, 0, z) })
        this.addComponent(transform)
        engine.addEntity(this)

        let shape = new BoxShape()
        //shape.withCollisions=true //@CF TODO

        for (let iz = 0; iz<zDepth; iz++){
            for (let ix = 0; ix<xWidth; ix++) {
              // create the entity
              let box = new Entity()
              let boxTransform = new Transform()
              boxTransform.position = new Vector3(0.5+ix, yHeight/2, 0.5+iz)
              boxTransform.scale = new Vector3(1, yHeight, 1)
              this.addComponent(boxTransform)
              this.addComponent(shape)
              this.addComponent(mtl)
              this.setParent(this)
              //engine.addEntity(box) // would get warning because it's already in the engine due to parent being in engine
            }
        }
*/

    }
}

///////////////////////////////////
// GroundsPath will always keep length, pathWidth and centerWidth as whole numbers >= 1
// If <1, makes them 1, then apply Math.floor
// Also if centerWidth < pathWidth and pathWidth-CenterWidth isn't even, then centerWidth will be reduced by 1
// The result is that all dimensions are whole meters, hence tileable as 1m boxes
// @CF todo above
class GroundCoverPath extends Entity {
    path: Entity
    leftSide: Entity
    rightSide: Entity
        
    constructor (x:number, z:number, bEast:boolean, length: number, pathWidth: number, centerWidth:number, centerHeight:number, sidesHeight:number, centerMaterial: Material, sidesMaterial: Material, addToEngine:boolean){
        super()
        const sidesWidth = (pathWidth-centerWidth)/2

        // Move this entity to the requesed location
        // This location will serve as the SW corner of the crosspath group
        const transform = new Transform({ position: new Vector3(x, 0, z) })
        this.addComponent(transform)

        engine.addEntity(this)

        this.leftSide = new GroundCover(0, (bEast?sidesWidth+centerWidth:0),  (bEast?length:sidesWidth), centerHeight, (bEast?sidesWidth:length),sidesMaterial, addToEngine)
        this.leftSide.setParent(this)

        this.path = new GroundCover((bEast?0:sidesWidth), (bEast?sidesWidth:0), (bEast?length:centerWidth), centerHeight, (bEast?centerWidth:length),centerMaterial, addToEngine)
        this.path.setParent(this)

        this.rightSide = new GroundCover((bEast?0:sidesWidth+centerWidth),(bEast?0:0),    (bEast?length:sidesWidth), centerHeight, (bEast?sidesWidth:length),sidesMaterial, addToEngine)
        this.rightSide.setParent(this)
    }
}

///////////////////////////////////
// GroundsCorssPath will always keep pathWidth and centerWidth as whole numbers >= 1
// If <1, makes them 1, then apply Math.floor
// Also if centerWidth < pathWidth and pathWidth-CenterWidth isn't even, then centerWidth will be reduced by 1
// The result is that all dimensions are whole meters, hence tileable as 1m boxes
// @CF todo above
class GroundCoverCrossPath extends Entity {
    nw:Entity
    n:Entity
    ne:Entity
    w:Entity
    c:Entity
    e:Entity
    sw:Entity
    s:Entity
    se:Entity

    constructor(x:number, z:number, pathWidth:number, centerWidth:number, centerHeight:number, sidesHeight:number, bWest: boolean, bEast: boolean, bSouth: boolean, bNorth: boolean, centerMaterial: Material, sidesMaterial: Material, addToEngine:boolean){
        super()
        // Move this entity to the requesed location
        // This location will serve as the SW corner of the crosspath group
        const transform = new Transform({ position: new Vector3(x, 0, z) })
        this.addComponent(transform)

        engine.addEntity(this)
         
        let sidesWidth = (pathWidth-centerWidth)/2

        this.sw = new GroundCover(0,0,  sidesWidth,sidesHeight,sidesWidth,   sidesMaterial, addToEngine) // SW corner
        this.sw.setParent(this)
        this.s = new GroundCover(sidesWidth, 0, centerWidth, sidesHeight,sidesWidth, (bSouth?centerMaterial:sidesMaterial), addToEngine) //South
        this.s.setParent(this)
        this.se = new GroundCover(sidesWidth+centerWidth, 0, sidesWidth, sidesHeight, sidesWidth, sidesMaterial, addToEngine) // SE corner
        this.se.setParent(this)
        this.w = new GroundCover(0, sidesWidth, sidesWidth, centerHeight,centerWidth, (bWest?centerMaterial:sidesMaterial), addToEngine) // West
        this.w.setParent(this)
        this.c = new GroundCover(sidesWidth,sidesWidth,  centerWidth, centerHeight, centerWidth, centerMaterial, addToEngine) // Center
        this.c.setParent(this)
        this.e = new GroundCover(sidesWidth+centerWidth, sidesWidth, sidesWidth, centerHeight,centerWidth, (bEast?centerMaterial:sidesMaterial), addToEngine) // East
        this.e.setParent(this)
        this.nw = new GroundCover(0, sidesWidth+centerWidth, sidesWidth, sidesHeight, sidesWidth, sidesMaterial, addToEngine) // NW corner
        this.nw.setParent(this)
        this.n = new GroundCover(sidesWidth,sidesWidth+centerWidth,centerWidth,centerHeight,sidesWidth,(bNorth?centerMaterial:sidesMaterial), addToEngine) // North
        this.n.setParent(this)
        this.ne = new GroundCover(sidesWidth+centerWidth, sidesWidth+centerWidth, sidesWidth, sidesHeight, sidesWidth, sidesMaterial, addToEngine) // NE Corner
        this.ne.setParent(this)
    }    
}